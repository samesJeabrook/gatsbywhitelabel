import styled from 'styled-components';
import { H1 } from '../../utils/fontStyles';
import { colours } from '../../config';

export const HeroWrapper = styled.section`
  align-items: center;
  background-image: url(${(props) => props.imagePath});
  background-size: cover;
  display: flex;
  flex-direction: column;
  height: ${(props) => (props.isFullHeight ? '100vh' : '60vh')};
  justify-content: center;
  position: relative;
  width: 100%;
`;

export const HeroGradient = styled.div`
  background-image: linear-gradient(
    rgba(251, 251, 251, 0),
    rgba(0, 0, 0, 0.7) 60%
  );
  height: 100%;
  left: 0;
  position: absolute;
  top: 0;
  width: 100%;
`;

export const HeroTextWrapper = styled.div`
  position: relative;
  z-index: 1;
`;

export const HeroHeading = styled(H1)`
  color: ${colours.white};
`;
