import React from 'react';
import PropTypes from 'prop-types';

import {
  HeroWrapper,
  HeroGradient,
  HeroTextWrapper,
  HeroHeading,
} from './hero.styled';

const Hero = ({ headingText = '' }) => (
  <HeroWrapper>
    <HeroGradient />
    <HeroTextWrapper>
      <HeroHeading>{headingText}</HeroHeading>
    </HeroTextWrapper>
  </HeroWrapper>
);

export default Hero;

Hero.propTypes = {
  headingText: PropTypes.string,
};

Hero.defaultProps = {
  headingText: '',
};
