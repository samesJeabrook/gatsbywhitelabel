import Hero from './hero/hero';
import Layout from './layout/layout';
import SEO from './seo/seo';

export { Hero, Layout, SEO };
