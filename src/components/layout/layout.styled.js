import styled from 'styled-components';

import { device, maxWidth } from '../../config';

export const LayoutContainer = styled.div`
  width: 100%;
  margin: 0 auto;

  @media (${device.tablet}) {
    max-width: ${maxWidth.tablet};
    background: red;
  }

  @media (${device.laptop}) {
    max-width: ${maxWidth.laptop};
  }

  @media (${device.desktop}) {
    max-width: ${maxWidth.desktop};
  }
`;
