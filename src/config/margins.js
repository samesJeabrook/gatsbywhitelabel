import { rem } from '../utils/styleUtils';

export const margins = {
  h1: rem(20),
  h2: rem(16),
  h3: rem(14),
  p: rem(5),
};
