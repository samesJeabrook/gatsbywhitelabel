import { colours } from './colours';
import { margins } from './margins';
import { device, maxWidth } from './device';

export { colours, margins, device, maxWidth };
