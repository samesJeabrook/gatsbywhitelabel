// convert pixels to rems
export const rem = (size) => {
  const remSize = size / 16;
  return `${remSize}rem`;
};
