import styled from 'styled-components';

export const H1 = styled.h1`
  font-family: 'Quicksand', sans-serif;
  font-size: 26px;
  font-weight: normal;
  line-height: 28px;
`;

export const H2 = styled.h2`
  font-family: 'Quicksand', sans-serif;
  font-size: 18px;
  font-weight: normal;
  line-height: 20px;
`;

export const P = styled.p`
  font-family: sans-serif;
  font-size: 14px;
  font-weight: normal;
  line-height: 16px;
`;
