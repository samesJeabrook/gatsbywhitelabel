import * as React from 'react';
// import { Link } from 'gatsby';

import GlobalStyles from '../utils/globalStyles';

import { Hero, Layout, SEO } from '../components';

const IndexPage = () => (
  <Layout>
    <GlobalStyles />
    <SEO title="Home" />
    <Hero headingText="Welcome" />
  </Layout>
);

export default IndexPage;
